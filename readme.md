# About

This source code contains all my exercises solved from the Algorithms, Part I and Algorithms, Part II courses created by Princeton University on the Coursera platform. Course leaders: Robert Sedgewick and Kevin Wayne.  

Coursera: [Algorithms, Part I](https://www.coursera.org/learn/algorithms-part1) and [Algorithms, Part II](https://www.coursera.org/learn/algorithms-part2)  

To run this project you must have:

* Installed Java 8 SDK.
* Add to classpath the [algs4.jar](http://algs4.cs.princeton.edu/code/algs4.jar) API  

### Style and Bug Checkers

**For Eclipse users:**

* [Checkstyle plugin](http://eclipse-cs.sourceforge.net/#!/) and a [Findbugs plugin](http://findbugs.cs.umd.edu/eclipse/).  

**See the Markdown page in Eclipse**

* [Markdown Text Editor](https://marketplace.eclipse.org/content/markdown-text-editor)  

**Eclipse - How import a java project from git repository?**

* File > Import > Git > Projects from Git > Existing local repository  
* Select a git repository window > Add > Choose the directory  

## 1st week

* Union-Find
* Analysis of Algorithms

[Programming Assignment 1: Percolation](http://coursera.cs.princeton.edu/algs4/assignments/percolation.html)  
[Programming Assignment 1 Checklist: Percolation](http://coursera.cs.princeton.edu/algs4/checklists/percolation.html)  
[Sample data: Percolation](http://coursera.cs.princeton.edu/algs4/testing/percolation-testing.zip)  

## 2nd week

* Stacks and Queues
* Elementary Sorts

[Programming Assignment 2: Deques and Randomized Queues](http://coursera.cs.princeton.edu/algs4/assignments/queues.html)  
[Programming Assignment 2 Checklist: Deques and Randomized Queues](http://coursera.cs.princeton.edu/algs4/checklists/queues.html)  
[Generics Q + A](http://algs4.cs.princeton.edu/13stacks/index.php#Q+A)  

## 3rd week

* Mergesort
* Quicksort

[Programming Assignment 3: Pattern Recognition](http://coursera.cs.princeton.edu/algs4/assignments/collinear.html)  
[Programming Assignment 3 Checklist: Pattern Recognition](http://coursera.cs.princeton.edu/algs4/checklists/collinear.html)  
[Topologically sweeping the arrangement of lines by Edelsbrunner and Guibas](http://www.hpl.hp.com/techreports/Compaq-DEC/SRC-RR-9.pdf)  
[Sample data: Collinear Points](http://coursera.cs.princeton.edu/algs4/testing/collinear-testing.zip)  

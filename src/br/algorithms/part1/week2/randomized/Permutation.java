package br.algorithms.part1.week2.randomized;

import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

import java.util.Iterator;

/**
 * Coursera: Algorithms, Part I - Princeton University (Robert Sedgewick, Kevin Wayne)
 * Programming Assignment 1: Percolation
 * Started in: September/2017
 * Permutation with random queue.
 * 
 * @author      Juliana Hohara de Souza Coelho
 * @version     %I%, %G%
 * @since       1.0
 * @see <a href="https://gitlab.com/juhohara">GitLab</a>
 * @see <a href="http://juhohara.gitlab.io">My personal site</a>
 * @see <a href="https://www.twitter.com/juhohara">Twitter</a>
 */
public class Permutation {
   
  /**
   * \brief Permutation with random queue.
   * 
   * @param args size 
   */
  public static void main(String[] args) {
    
    // Assume that 0 <= k <= n, where n is the number of string on standard input. 
    int k = Integer.parseInt(args[0]);
    
    RandomizedQueue<String> randomQueue = new RandomizedQueue<String>();
    
    while (!StdIn.isEmpty()) {
        
      randomQueue.enqueue(StdIn.readString());
    
    }
    
    Iterator<String> it = randomQueue.iterator();      
    for (int i = 0; i < k; ++i) {
        
      StdOut.println(it.next());
    
    }
  
  }
  
}
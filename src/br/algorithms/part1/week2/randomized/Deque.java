package br.algorithms.part1.week2.randomized;

import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Coursera: Algorithms, Part I - Princeton University (Robert Sedgewick, Kevin Wayne)
 * Programming Assignment 1: Percolation
 * Started in: September/2017
 * Deque data structure.
 * 
 * @author      Juliana Hohara de Souza Coelho
 * @version     %I%, %G%
 * @since       1.0
 * @see <a href="https://gitlab.com/juhohara">GitLab</a>
 * @see <a href="http://juhohara.gitlab.io">My personal site</a>
 * @see <a href="https://www.twitter.com/juhohara">Twitter</a>
 */

public class Deque<Item> implements Iterable<Item> {

  private Item[] deque;
  private int dequeSize = 0;
  private int head = 0;
  
  /**
   * \brief Construct an empty deque.
   */
  public Deque() {
    
    deque = (Item[]) new Object[1];
    dequeSize = 0;
    head = 0;
    
  }
  
  /**
   * \brief Is the deque empty?.
   * @return true if empty, false otherwise
   */
  public boolean isEmpty() {
    
    if (dequeSize > 0) {
      
      return false;
      
    }
    
    return true;
    
  }
 
  /**
   * \brief Return the number of items on the deque.
   * 
   * @return deque size
   */
  public int size() {
    
    return dequeSize;
    
  }
  
  /**
   * \brief Add the item to the front.
   * 
   * @param item new item
   */  
  public void addFirst(Item item) {
    
    if (item == null) {
      
      throw new IllegalArgumentException();
      
    }
    
    resizeOnAdd();
    
    if (head == 0) {
      
      deque[(head = deque.length - 1)] = item;
      
    } else {
      
      deque[(--head) % deque.length] = item;
      
    }
    
    ++dequeSize;
        
  }
  
  /**
   * \brief Add the item to the end.
   * 
   * @param item add new item at last
   */
  public void addLast(Item item) {
    
    if (item == null) {
      
      throw new IllegalArgumentException();
      
    }

    resizeOnAdd();
    deque[(head + dequeSize++) % deque.length] = item;

  }
  
  /**
   * \brief Remove and return the item from the front.
   * @return front item
   */
  public Item removeFirst() {
    
    if (isEmpty()) {
      
      throw new NoSuchElementException();
    
    }
    
    --dequeSize;
    
    Item item = deque[head];
    deque[head] = null;
    head = (head + 1) % deque.length;
    
    resizeOnRemove();
    
    return item;
    
  }
  
  /**
   * \brief Remove and return the item from the end.
   * 
   * @return last item
   */
  public Item removeLast() {
    
    if (isEmpty()) {
      
      throw new NoSuchElementException();
    
    }

    int index = (head + dequeSize - 1) % deque.length;
    
    --dequeSize;
    
    Item item = deque[index];
    deque[index] = null;
    
    resizeOnRemove();
    
    return item;
    
  }
  
  /**
   * \brief In the addition of the item resize if necessary.
   */
  private void resizeOnAdd() {
    
    if (dequeSize == deque.length) {
      
      resize(2 * deque.length);
      
    }

  }
  
  /**
   * \brief In the removal of the item resize if necessary.
   */  
  private void resizeOnRemove() {
    
    if (dequeSize > 0 && dequeSize == deque.length / 4) {
      
      resize(deque.length / 2);
      
    }
    
  }
  
  /**
   * \brief Resize deque.
   * 
   * @param capacity new size
   */  
  private void resize(int capacity) {
        
    Item[] copy = (Item[]) new Object[capacity];
    
    for (int i = 0; i < dequeSize; ++i) {
      
      copy[i] = deque[(head + i) % deque.length];
      
    }
    
    deque = copy;
    head = 0;
     
  }
  
  /**
   * \brief Return an iterator over items in order from front to end.
   */
  public Iterator<Item> iterator() {
    
    return new DequeArrayIterator();
    
  }
  
  /**
   * Coursera: Algorithms, Part I - Princeton University (Robert Sedgewick, Kevin Wayne)
   * Programming Assignment 1: Percolation
   * Started in: September/2017
   * Iterator over items in order from front to end.
   * 
   * @author      Juliana Hohara de Souza Coelho
   * @version     %I%, %G%
   * @since       1.0
   * @see <a href="https://gitlab.com/juhohara">GitLab</a>
   * @see <a href="http://juhohara.gitlab.io">My personal site</a>
   * @see <a href="https://www.twitter.com/juhohara">Twitter</a>
   *
   */
  private class DequeArrayIterator implements Iterator<Item> {
    
    private int index = 0;
    
    /**
     * \brief Returns true if the iteration has more elements.
     */
    public boolean hasNext() { 
      
      return index < dequeSize; 
    
    }
    
    /**
     * \brief Not implemented.
     */
    public void remove() {
      
      throw new UnsupportedOperationException(); 
      
    }
    
    /**
     * \brief Returns the next element in the iteration.
     */
    public Item next() { 
      
      if (!hasNext()) {
        
        throw new NoSuchElementException();
      
      }
      
      Item item = deque[(index + head) % deque.length];
      ++index;
      return item;
      
    }
    
  }
  
  /**
   * \brief Unit testing (optional).
   * 
   * @param args arguments
   */
  public static void main(String[] args) {
    
    Deque<String> dq = new Deque<String>();
    
    while (!StdIn.isEmpty()) {
        
      String item = StdIn.readString();
      if (!item.equals("-")) {
        
        dq.addLast(item);
        
      } else if (!dq.isEmpty()) {
        
        StdOut.print(dq.removeLast() + " ");
      
      }
      
    }
    
    StdOut.println("(" + dq.size() + " left on queue)");

    StdOut.printf("----------------------\n");

    for (String s : dq) {
      
      StdOut.printf("%s \n", s);
        
    }
                
  }
  
}

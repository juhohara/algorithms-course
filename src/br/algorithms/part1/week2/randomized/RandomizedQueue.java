package br.algorithms.part1.week2.randomized;

import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Coursera: Algorithms, Part I - Princeton University (Robert Sedgewick, Kevin Wayne)
 * Programming Assignment 1: Percolation
 * Started in: September/2017
 * Randomized queue data structure.
 * 
 * @author      Juliana Hohara de Souza Coelho
 * @version     %I%, %G%
 * @since       1.0
 * @see <a href="https://gitlab.com/juhohara">GitLab</a>
 * @see <a href="http://juhohara.gitlab.io">My personal site</a>
 * @see <a href="https://www.twitter.com/juhohara">Twitter</a>
 */
public class RandomizedQueue<Item> implements Iterable<Item> {
  
  private Item[] queue;
  private int queueSize = 0;
  private int tail = 0;
  
  /**
   * \brief Construct an empty randomized queue.
   */
  public RandomizedQueue() {
    
    queue = (Item[]) new Object[2];
    queueSize = 0;
    tail = 0;
    
  }
  
  /**
   * \brief Is the queue empty?.
   * 
   * @return if true is empty, false otherwise
   */
  public boolean isEmpty() {
    
    if (queueSize > 0) {
      
      return false;
      
    }
    
    return true;
    
  }
  
  /**
   * \brief Return the number of items on the queue.
   * 
   * @return randomized queue size
   */
  public int size() {
    
    return queueSize;
    
  }
  
  /**
   * \brief Add the item.
   * 
   * @param item add new item
   */
  public void enqueue(Item item) {
    
    if (item == null) {
      
      throw new IllegalArgumentException();
      
    }

    resizeOnAdd();
    queue[tail++] = item;
    ++queueSize;
  }
  
  /**
   * \brief Remove and return a random item.
   * 
   * @return random item
   */
  public Item dequeue() {
    
    if (isEmpty()) {
      
      throw new NoSuchElementException();
    
    }
    
    tail--;
    int r = StdRandom.uniform(queueSize);
    Item item = swap(r, tail);
    --queueSize;
        
    resizeOnRemove();
    
    return item;
    
  }
  
  /**
   * \brief Return (but do not remove) a random item.
   * 
   * @return random item
   */
  public Item sample() {
    
    int r = StdRandom.uniform(queueSize);    
    return queue[r];
    
  }
  
  /**
   * \brief return an independent iterator over items in random order.
   */
  public Iterator<Item> iterator() {
    
    return new RandomArrayIterator();
    
  }
  
  /**
   * \brief Swap item positions.
   * 
   * @param index1 current item
   * @param index2 item to null
   * 
   * @return item in the current position
   * 
   */
  private Item swap(int index1, int index2) {
    
    Item item = queue[index1]; 
    queue[index1] = queue[index2];
    queue[index2] = null;
    
    return item;    
  }
  
  /**
   * \brief In the addition of the item resize if necessary.
   */
  private void resizeOnAdd() {
    
    if (queueSize == queue.length) {
      
      resize(2 * queue.length);
      
    }

  }
  
  /**
   * \brief In the removal of the item resize if necessary.
   */    
  private void resizeOnRemove() {
    
    if (queueSize > 0 && queueSize == queue.length / 4) {
      
      resize(queue.length / 2);
      
    }
    
  }
  
  /**
   * \brief Resize deque.
   * 
   * @param capacity new size
   */ 
  private void resize(int capacity) {
    
    Item[] copy = (Item[]) new Object[capacity];
    
    for (int i = 0; i < queueSize; ++i) {
      
      copy[i] = queue[(tail + i) % queue.length];
    }
    
    queue = copy;
    tail  = queueSize;
     
  }
  
  /**
   * Coursera: Algorithms, Part I - Princeton University (Robert Sedgewick, Kevin Wayne)
   * Programming Assignment 1: Percolation
   * Started in: September/2017
   * Independent iterator over items in random order.
   * 
   * @author      Juliana Hohara de Souza Coelho
   * @version     %I%, %G%
   * @since       1.0
   * @see <a href="https://gitlab.com/juhohara">GitLab</a>
   * @see <a href="http://juhohara.gitlab.io">My personal site</a>
   * @see <a href="https://www.twitter.com/juhohara">Twitter</a>
   *
   */
  private class RandomArrayIterator implements Iterator<Item> {
    
    private int index = 0;
    private Item[] copy;
    
    /**
     * \ Constructor.
     */
    public RandomArrayIterator() {      
      
      copy = (Item[]) new Object[queueSize];
      for (int i = 0; i < queueSize; i++) {
        copy[i] = queue[i];
      }
      
      // random array
      StdRandom.shuffle(copy);
      
    }
    
    /**
     * \brief Returns true if the iteration has more elements.
     */
    public boolean hasNext() { 
      
      return index < queueSize; 
    
    }
    
    /**
     * \brief Not implemented.
     */
    public void remove() {
      
      throw new UnsupportedOperationException(); 
      
    }
    
    /**
     * \brief Returns the next element in the iteration.
     */
    public Item next() { 
      
      if (!hasNext()) {
        
        throw new NoSuchElementException();
      
      }
      
      Item item = copy[index];
      ++index;
      return item;
      
    }
    
  }
  
  /**
   * \brief Unit testing (optional).
   * 
   * @param args arguments
   */
  public static void main(String[] args) {
    
    RandomizedQueue<String> rdq = new RandomizedQueue<String>();
    
    while (!StdIn.isEmpty()) {
      
      String item = StdIn.readString();
      if (!item.equals("-")) {
        
        rdq.enqueue(item);
          
      } else if (!rdq.isEmpty()) {
        
        StdOut.print(rdq.dequeue() + " ");
        
      }
        
    }
    StdOut.println("(" + rdq.size() + " left on queue)");
    
    StdOut.printf("----------------------\n");
    
    for (String s : rdq) {
      
      StdOut.printf("%s \n", s);
        
    }
    
  }

}
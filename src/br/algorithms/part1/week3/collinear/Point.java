package br.algorithms.part1.week3.collinear;

import java.util.Comparator;

public class Point implements Comparable<Point> {
  
  /**
   * 
   * @param x
   * @param y
   */
  public Point(int x, int y) { // constructs the point (x, y)
     
  }
  
  /**
   * 
   */
  public void draw() { // draws this point
     
  }
   
  /**
   * 
   * @param that
   */
  public void drawTo(Point that) { // draws the line segment from this point to that point
     
  }
   
  /**
   * 
   */
  public String toString() { // string representation
     
    return new String();
  }
  
  /**
   * 
   */
  //compare two points by y-coordinates, breaking ties by x-coordinates
  public int compareTo(Point that) { 
    
    return 0;
  }
   
  /**
   * 
   * @param that
   * @return
   */
  public double slopeTo(Point that) { // the slope between this point and that point
     
    return 0.;
  }
   
  /**
   * 
   * @return
   */
  public Comparator<Point> slopeOrder() { // compare two points by slopes they make with this point
    
    Point p = new Point(0, 0);    
    return (Comparator<Point>) p;
  }
}
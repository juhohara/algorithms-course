
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;


/**
 * Coursera: Algorithms, Part I - Princeton University (Robert Sedgewick, Kevin Wayne)
 * Programming Assignment 1: Percolation
 * Started in: August/2017
 * Program to estimate the value of the percolation threshold via Monte Carlo simulation. 
 * 
 * @author      Juliana Hohara de Souza Coelho
 * @version     %I%, %G%
 * @since       1.0
 * @see <a href="https://gitlab.com/juhohara">GitLab</a>
 * @see <a href="http://juhohara.gitlab.io">My personal site</a>
 * @see <a href="https://www.twitter.com/juhohara">Twitter</a>
 */

public class PercolationStats {

  private double[] experiments;
    
  /**
   * \brief perform trials independent experiments on an n-by-n grid.
   */
  public PercolationStats(int n, int trials) { 
        
    init(n, trials);
        
  }
    
  /**
   * \brief create n objetcs with n x n grids.
   */
  private void init(int n, int trials) {
                
    if (isIllegalArgument(n, trials)) {
            
      throw new IllegalArgumentException();
            
    }

    Percolation[] percolations = new Percolation[trials];
       
    experiments = new double[trials];
       
    for (int i = 0 ; i < trials ; ++i) {
           
      percolations[i] = new Percolation(n);           
      monteCarloSimulation(i, n, percolations[i]);
           
    }
       
  }
    
  /**
   * \brief Monte Carlo simulations are based on generating physical states 
   * of a given system with a desired probability distribution by the use of random numbers.
   */
    
  private void monteCarloSimulation(int index, int n, Percolation percolation) {
        
    int row = 0;
    int col = 0;
        
    while (!percolation.percolates()) {
            
      row = StdRandom.uniform(1, n + 1);
      col = StdRandom.uniform(1, n + 1);
                           
      percolation.open(row, col);     
            
    }
        
    experiments[index] = 1.0 * percolation.numberOfOpenSites() / (n * n);
  }
    
  /**
   * \brief sample mean of percolation threshold.
   */
  public double mean() { 
        
    return StdStats.mean(experiments);
        
  }
    
  /**
   * \brief sample standard deviation of percolation threshold.
   */
  public double stddev() {
        
    return StdStats.stddev(experiments);
        
  }
    
  /**
   * \brief low  endpoint of 95% confidence interval.
   */
  public double confidenceLo() {
        
    return mean() - 1.96 * stddev() / Math.sqrt(experiments.length);
        
  }
    
  /**
   * \brief high endpoint of 95% confidence interval.
   */
  public double confidenceHi() { 
        
    return mean() + 1.96 * stddev() / Math.sqrt(experiments.length);
        
  }
    
  /**
   * \brief check if is illegal argument.
   */
  private boolean isIllegalArgument(int n, int trials) {
        
    if (n <= 0 || trials <= 0) {
            
      return true;
            
    }
        
    return false;
        
  }
    
  /**
   * \brief test client (described below).
   */
  public static void main(String[] args) { 
        
    int n = Integer.parseInt(args[0]);
    int t = Integer.parseInt(args[1]);
        
    PercolationStats percolationStats = new PercolationStats(n, t);
    StdOut.printf("mean.....................= %f\n", percolationStats.mean());
    StdOut.printf("stddev...................= %f\n", percolationStats.stddev());
    StdOut.println("95% confidence interval..= " + percolationStats.confidenceLo() 
        + ", " + percolationStats.confidenceHi());
        
  }
}

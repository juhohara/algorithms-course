
import edu.princeton.cs.algs4.WeightedQuickUnionUF;

/**
 * Coursera: Algorithms, Part I - Princeton University (Robert Sedgewick, Kevin Wayne)
 * Programming Assignment 1: Percolation
 * Started in: August/2017
 * We model a percolation system using an n-by-n grid of sites. 
 * Each site is either open or blocked. 
 * A full site is an open site that can be connected to 
 * an open site in the top row via a chain of neighboring 
 * (left, right, up, down) open sites. 
 * We say the system percolates if there is a full site in the bottom row. 
 * In other words, a system percolates 
 * if we fill all open sites connected to the top row and that process 
 * fills some open site on the bottom row.
 * 
 * @author      Juliana Hohara de Souza Coelho
 * @version     %I%, %G%
 * @since       1.0
 * @see <a href="https://gitlab.com/juhohara">GitLab</a>
 * @see <a href="http://juhohara.gitlab.io">My personal site</a>
 * @see <a href="https://www.twitter.com/juhohara">Twitter</a>
 */

public class Percolation {

  private boolean[][]             sites;
  private int                     gridSize = 0;
  private int                     top = 0;
  private int                     bottom = 0;
  private int                     numberOpenSites = 0;
  private WeightedQuickUnionUF    quickFind;
  
  /**
   * \brief create n-by-n grid, with all sites blocked.
   */
  public Percolation(int n) {            
        
    gridSize = n;
    bottom = gridSize * gridSize + 1; 
    quickFind = new WeightedQuickUnionUF(gridSize * gridSize + 2);
    sites = new boolean[gridSize][gridSize];
        
  }
  
  /**
   * \brief open site (row, col) if it is not open already.
   */   
  public void open(int row, int col) {
    
    if (isOutOfBounds(row, col)) {
            
      throw new IndexOutOfBoundsException();
            
    }
        
    if (isOpen(row, col)) {
            
      return;
            
    }
       
    ++numberOpenSites;
       
    sites[row - 1][col - 1] = true;
       
    // open sites
       
    openTop(row, col);
    openBottom(row, col);
    openLeft(row, col);
    openRight(row, col);
    openUp(row, col);
    openDown(row, col);
       
  }
   
  /**
   * \brief is site (row, col) open?.
   * */   
  public boolean isOpen(int row, int col) { 
        
    if (isOutOfBounds(row, col)) {
            
      throw new IndexOutOfBoundsException();
            
    }
        
    return sites[row - 1][col - 1];
        
  }
       
  /**
   * \brief is site (row, col) full?.
   */   
  public boolean isFull(int row, int col) {
        
    if (isOutOfBounds(row, col)) {
      
      throw new IndexOutOfBoundsException();
            
    }
        
    return quickFind.connected(top, getIndex(row , col));
        
  }
   
  /**
   * \brief number of open sites.
   */
  public int numberOfOpenSites() {     
        
    return numberOpenSites;
        
  }
    
  /**
   * \brief does the system percolate?.
   */
  public boolean percolates() {
        
    return quickFind.connected(top, bottom);
        
  }
    
  /**
   * \brief get site index.
   */
  private int getIndex(int row, int col) {
        
    return gridSize * (row - 1) + col;
       
  }
    
  /**
   * \brief check if is out of bounds.
   */
  private boolean isOutOfBounds(int row, int col) {
        
    if ((row < 0) || (col < 0) || (row > gridSize) || (col > gridSize)) {
            
      return true;
            
    }
        
    return false;
        
  }
    
  /**
   * \brief Open site on top.
   */
  private boolean openTop(int row, int col) {
        
    if (row == 1) {
            
      quickFind.union(getIndex(row, col), top);
      return true;
           
    }
        
    return false;
        
  }
    
  /**
   * \brief Open site on bottom.
   */
  private boolean openBottom(int row, int col) {
        
    if (row == gridSize) {
            
      quickFind.union(getIndex(row, col), bottom);
      return true;
            
    }
        
    return false;
        
  }
    
  /**
     * \brief Open site on left.
     */
  private boolean openLeft(int row, int col) {
        
    if (col > 1 && isOpen(row, col - 1)) {
            
      quickFind.union(getIndex(row, col), getIndex(row, col - 1));
      return true;
            
    }
        
    return false;
  }
    
  /**
   * \brief Open site on right.
   */
  private boolean openRight(int row, int col) {
        
    if (col < gridSize && isOpen(row, col + 1)) {
            
      quickFind.union(getIndex(row, col), getIndex(row, col + 1));
      return true;
            
    }
        
    return false;
        
  }
    
  /**
   * \brief Open site on up.
   */
  private boolean openUp(int row, int col) {
        
    if (row > 1 && isOpen(row - 1, col)) {
            
      quickFind.union(getIndex(row, col), getIndex(row - 1, col));
      return true;
            
    }
        
    return false;
        
  }
    
  /**
   * \brief Open site on down.
   */
  private boolean openDown(int row, int col) {
        
    if (row < gridSize && isOpen(row + 1, col)) {
            
      quickFind.union(getIndex(row, col), getIndex(row + 1, col));
      return true;
            
    }
        
    return false;
        
  }
    
}
